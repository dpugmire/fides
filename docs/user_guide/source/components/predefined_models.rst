.. _data-model-generation:

*********************
Data Model Generation
*********************

Fides can now generate a data model based on some attributes that are written by ADIOS, instead of requiring the user to write their own data model.
Currently they must be written as ADIOS ``Attributes`` and not ``Variables``, and they can either be written in the same file as your ADIOS data or you could have ADIOS write them in a separate file that contains only the attributes.

.. note::

    If the attributes are written in a file separate from the actual data, Fides expects that it is in the same directory as the data to be read.

General Attributes
==================

This attribute can be used with any data model to use one of your ADIOS Variables as the time values.
For instance, in ParaView (whether it's post-processing or using Catalyst), specifying the time variable will use the values in the specified array instead of just the ADIOS step numbers.

.. list-table:: Attributes
   :header-rows: 1

   * - Attribute Name
     - Possible types/values
     - Default
   * - ``Fides_Time_Variable``
     - string: name of variable to use for time
     - none


Supported Data Models
=====================

For each supported data model there is a table of attributes and possible types/values listed below.
If an attribute does not list a default value, it must be specified in the data given to Fides.

Uniform Data Model
------------------

The data model uses uniform point coordinates for the coordinate system,
needing the origin and spacing to be specified.
The cell set is structured based on the dimensions of the data.

.. list-table:: Attributes
   :header-rows: 1

   * - Attribute Name
     - Possible types/values
     - Default
   * - ``Fides_Data_Model``
     - string: ``uniform``
     - none
   * - ``Fides_Origin``
     - 3 integer or floating points
     - none
   * - ``Fides_Spacing``
     - 3 integer or floating points
     - none
   * - ``Fides_Dimension_Variable``
     - string: name of variable to use for determining dimensions
     - none

**Example JSON**

The following JSON shows an example of what Fides generates for this data model.

.. code-block:: json

    {
        "uniform_grid": {
            "data_sources": [
                {
                    "name": "source",
                    "filename_mode": "input"
                }
            ],
            "coordinate_system": {
                "array": {
                    "array_type": "uniform_point_coordinates",
                    "dimensions": {
                        "source": "variable_dimensions",
                        "data_source": "source",
                        "variable": "density"
                    },
                    "origin": {
                        "source": "array",
                        "values": [
                            0,
                            0,
                            0
                        ]
                    },
                    "spacing": {
                        "source": "array",
                        "values": [
                            0.1,
                            0.1,
                            0.1
                        ]
                    }
                }
            },
            "cell_set": {
                "cell_set_type": "structured",
                "dimensions": {
                    "source": "variable_dimensions",
                    "data_source": "source",
                    "variable": "density"
                }
            }
        }
    }


Rectilinear Data Model
----------------------

This data model creates a rectilinear data model where the coordinate system
is specified by a cartesian product of (separate) arrays for the x, y, and z.
The cell set is structured based on the dimensions of the data.

.. list-table:: Attributes
   :header-rows: 1

   * - Attribute Name
     - Possible types/values
     - Default
   * - ``Fides_Data_Model``
     - string: ``rectilinear``
     - none
   * - ``Fides_X_Variable``
     - string: name of variable representing x values
     - ``x``
   * - ``Fides_Y_Variable``
     - string: name of variable representing y values
     - ``y``
   * - ``Fides_Z_Variable``
     - string: name of variable representing z values
     - ``z``
   * - ``Fides_Dimension_Variable``
     - string: name of variable to use for determining dimensions
     - none

**Example JSON**

The following JSON shows an example of what Fides generates for this data model.

.. code-block:: json

    {
        "rectilinear_grid": {
            "data_sources": [
                {
                    "name": "source",
                    "filename_mode": "input"
                }
            ],
            "coordinate_system": {
                "array": {
                    "array_type": "cartesian_product",
                    "x_array": {
                        "array_type": "basic",
                        "data_source": "source",
                        "variable": "x",
                        "static": true
                    },
                    "y_array": {
                        "array_type": "basic",
                        "data_source": "source",
                        "variable": "y",
                        "static": true
                    },
                    "z_array": {
                        "array_type": "basic",
                        "data_source": "source",
                        "variable": "z",
                        "static": true
                    }
                }
            },
            "cell_set": {
                "cell_set_type": "structured",
                "dimensions": {
                    "source": "variable_dimensions",
                    "data_source": "source",
                    "variable": "scalars"
                }
            }
        }
    }

Unstructured Data Model
-----------------------

An unstructured or explicit mesh that expects arrays containing the coordinates, connectivity, cell types,
and number of vertices for each cell.

.. list-table:: Attributes
   :header-rows: 1

   * - Attribute Name
     - Possible types/values
     - Default
   * - ``Fides_Data_Model``
     - string: ``unstructured``
     - none
   * - ``Fides_Coordinates_Variable``
     - string: name of variable containing coordinates
     - ``points``
   * - ``Fides_Connectivity_Variable``
     - string: name of connectivity variable
     - ``connectivity``
   * - ``Fides_Cell_Types_Variable``
     - string: name of cell types variable
     - ``cell_types``
   * - ``Fides_Num_Vertices_Variable``
     - string: name of variable listing number of vertices for each cell
     - ``num_verts``

**Example JSON**

The following JSON shows an example of what Fides generates for this data model.

.. code-block:: json

    {
        "unstructured_grid": {
            "data_sources": [
                {
                    "name": "source",
                    "filename_mode": "input"
                }
            ],
            "coordinate_system": {
                "array": {
                    "array_type": "basic",
                    "data_source": "source",
                    "variable": "points",
                    "static": true
                }
            },
            "cell_set": {
                "cell_set_type": "explicit",
                "connectivity": {
                    "array_type": "basic",
                    "data_source": "source",
                    "variable": "connectivity"
                },
                "cell_types": {
                    "array_type": "basic",
                    "data_source": "source",
                    "variable": "cell_types"
                },
                "number_of_vertices": {
                    "array_type": "basic",
                    "data_source": "source",
                    "variable": "num_verts"
                }
            }
        }
    }

Unstructured with Single Cell Type Data Model
---------------------------------------------

Similar to unstructured, except that there is only a single cell type used in the data, so we don't
need to store arrays of the cell types and number of vertices.

.. list-table:: Attributes
   :header-rows: 1

   * - Attribute Name
     - Possible types/values
     - Default
   * - ``Fides_Data_Model``
     - string: ``unstructured_single``
     - none
   * - ``Fides_Cell_Type``
     - string: one of ``vertex``, ``line``, ``triangle``, ``quad``, ``tetrahedron``, ``hexahedron``, ``wedge``, ``pyramid``
     - none
   * - ``Fides_Coordinates_Variable``
     - string: name of variable containing coordinates
     - ``points``
   * - ``Fides_Connectivity_Variable``
     - string: name of connectivity variable
     - ``connectivity``

**Example JSON**

The following JSON shows an example of what Fides generates for this data model.

.. code-block:: json

    {
        "unstructured_grid_single_cell_type": {
            "data_sources": [
                {
                    "name": "source",
                    "filename_mode": "input"
                }
            ],
            "coordinate_system": {
                "array": {
                    "array_type": "basic",
                    "data_source": "source",
                    "variable": "points",
                    "static": true
                }
            },
            "cell_set": {
                "cell_set_type": "single_type",
                "cell_type": "triangle",
                "data_source": "source",
                "variable": "connectivity",
                "static": true
            }
        }
    }

XGC Data Model
--------------

.. list-table:: Attributes
   :header-rows: 1

   * - Attribute Name
     - Possible types/values
     - Default
   * - ``Fides_Data_Model``
     - string: ``xgc``
     - none
   * - ``Fides_XGC_Mesh_Filename``
     - string: filename of mesh data (not full path)
     - ``xgc.mesh.bp``
   * - ``Fides_XGC_3d_Filename``
     - string: filename of 3d data (not full path)
     - ``xgc.3d.bp``
   * - ``Fides_XGC_Diag_Filename``
     - string: filename of diag data (not full path)
     - ``xgc.oneddiag.bp``
   * - ``Fides_Coordinates_Variable``
     - string: name of plane coordinates variable
     - ``rz``
   * - ``Fides_Triangle_Connectivity_Variable``
     - string: name of variable containing triangle connectivity
     - ``nd_connect_list``
   * - ``Fides_Plane_Connectivity_Variable``
     - string: name of variable containing connectivity between planes
     - ``nextnode``
   * - ``Fides_Number_Of_Planes_Variable``
     - string: name of variable for number of planes
     - ``nphi``

**Example JSON**

The following JSON shows an example of what Fides generates for this data model.

.. code-block:: json

    {
        "xgc": {
            "data_sources": [
                {
                    "name": "mesh",
                    "filename_mode": "relative",
                    "filename": "xgc.mesh.bp"
                },
                {
                    "name": "3d",
                    "filename_mode": "relative",
                    "filename": "xgc.3d.bp"
                },
                {
                    "name": "diag",
                    "filename_mode": "relative",
                    "filename": "xgc.oneddiag.bp"
                }
            ],
            "coordinate_system": {
                "array": {
                    "array_type": "xgc_coordinates",
                    "data_source": "mesh",
                    "variable": "rz",
                    "static": true,
                    "is_cylindrical": false
                }
            },
            "cell_set": {
                "cell_set_type": "xgc",
                "periodic": true,
                "cells": {
                    "array_type": "basic",
                    "data_source": "mesh",
                    "variable": "nd_connect_list",
                    "static": true,
                    "is_vector": "false"
                },
                "plane_connectivity": {
                    "array_type": "basic",
                    "data_source": "mesh",
                    "variable": "nextnode",
                    "static": true,
                    "is_vector": "false"
                }
            },
            "fields": [
                {
                    "variable_list_attribute_name": "Fides_Variable_List",
                    "variable_association_attribute_name": "Fides_Variable_Associations",
                    "variable_sources_attribute_name": "Fides_Variable_Sources",
                    "variable_arrays_attribute_name": "Fides_Variable_Array_Types",
                    "array": {
                        "array_type": "",
                        "data_source": "",
                        "variable": ""
                    }
                }
            ],
            "number_of_planes": {
                "source": "scalar",
                "data_source": "3d",
                "variable": "nphi"
            }
        }
    }
