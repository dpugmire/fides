#!/bin/sh

set -e

case "$( uname -s )" in
    Linux)
        shatool="sha256sum"
        version="3.26.3"
        platform="linux-x86_64"
        sha256sum="28d4d1d0db94b47d8dfd4f7dec969a3c747304f4a28ddd6fd340f553f2384dc2"
        prefix="cmake"
        ;;
    *)
        echo "Unrecognized platform $( uname -s )"
        exit 1
        ;;
esac
readonly shatool
readonly version
readonly platform
readonly sha256sum
readonly prefix

filename="cmake-${version}-${platform}"
tarball="${filename}.tar.gz"
readonly filename
readonly tarball

#install cmake
cd .gitlab

curl -OL "https://github.com/Kitware/CMake/releases/download/v${version}/${tarball}"
echo "${sha256sum}  ${tarball}" > cmake.sha256sum
$shatool --check cmake.sha256sum
tar xzf "${tarball}"
mv "${filename}" "${prefix}"
